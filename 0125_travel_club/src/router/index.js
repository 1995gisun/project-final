import VueRouter from 'vue-router';
import DefaultPage from '../components/page/DefaultPage.vue'
import Login from '../components/page/Login.vue'
import RegisterPage from '../components/page/RegisterPage.vue'
import ProductPage from '../components/page/ProductPage.vue'
import MyInfoPage from '../components/page/MyInfoPage.vue'
import ProductListPage from '../components/page/ProductListPage.vue'
import OrderPage from '../components/page/OrderPage.vue'
import ShoppingList from '../components/page/ShoppingList.vue'

const routes = [

    { path: '/', component: DefaultPage},
    { path: '/default', component: DefaultPage},
    { path: '/login', component: Login},
    { path: '/register', component: RegisterPage},
    { path: '/product', component: ProductPage},
    { path: '/myInfo', component: MyInfoPage},
    { path: '/productList', component: ProductListPage},
    { path: '/order', component: OrderPage},
    { path: '/bucket', component: ShoppingList},





];



export default new VueRouter({
    routes: routes
})