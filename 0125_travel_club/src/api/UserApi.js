import AxiosClient from "./AxiosClient";

class UserApi {
    constructor() {
        this.client = new AxiosClient('api/user')
    }

    async registerUser(user) {    
        return this.client.post('/create', user);
    }

    async findOnlyEmail(email) {
        const data = (await this.client.get('/find-email', {email: email})).data;
        console.log(data);

        return data;
    }

    async findUser(email, password){
        const data = (await this.client.get('/find', {email: email, password: password})).data;
        console.log(data);

        return data;
    }

    async modify(user){
        return this.client.post('/modify', user);
    }

    async remove(email){
        return this.client.get('/remove', email);
    }


}

const instance = new UserApi();

export default instance;
