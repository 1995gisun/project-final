import AxiosClient from "./AxiosClient";

class ProductApi {
    constructor() {
        this.client = new AxiosClient('api/product')
    }

    async registerProduct(product) {    
        return this.client.post('/create', product);
    }

    async findProduct(productId){
        const data = (await this.client.get('/find-id', {productId: productId})).data;

        return data;
    }

    async findProductByName(productName){
        const data = (await this.client.get('/find-name', {productName: productName})).data;

        return data;
    }

    async findProductList(){
        const data= (await this.client.get('/find-product-list', {})).data;
        // console.log(data);
        return data;
    }

    async modify(product){
        return this.client.post('/modify', product);
    }

    async remove(productId){
        const data = (await this.client.get('/remove', {productId: productId})).data;
        return data;
    }


}

const instance = new ProductApi();

export default instance;
