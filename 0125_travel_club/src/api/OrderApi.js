import AxiosClient from "./AxiosClient";

class OrderApi {

    constructor(){
        this.client = new AxiosClient('api/order')
    }

    async registerOrder(order) {   
        console.log(order); 
        const data = (await this.client.post('/create-order', order)).data;
        console.log(data);
        // return this.client.post('/create-order', order);
        return data;
    }

    async findOrder(orderNum){
        const data = (await this.client.get('/find', {orderNum: orderNum})).data;
        console.log(data);

        return data;
    }

    async findOrderByEmail(userEmail){
        const data = (await this.client.get('/find-email', {userEmail: userEmail})).data;
        return data;
    }


    async modify(order){
        return this.client.post('/modify', order);
    }

    async remove(orderNum){
        const data = (await this.client.get('/remove', {orderNum: orderNum})).data;
        return data;
    }


}

const instance = new OrderApi();

export default instance;