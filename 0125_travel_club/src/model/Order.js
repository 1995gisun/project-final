
class Order {
    constructor(userEmail, productId){
		var moment = require('moment');


		this.orderNum = 0;
		this.userEmail = userEmail;
		this.productId = productId;
		// this.Product = Product;
		this.status = 0;
		this.count = 1;
		this.totalPrice = 0;
		this.address = "address";
		this.date = moment().format('YYYY-MM-DD');
    }
}
export default Order;