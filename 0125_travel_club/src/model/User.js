
class User {
    constructor(email, password, phoneNumber, address){
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.role = 'member';
    }
}
export default User;