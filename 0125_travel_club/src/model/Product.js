
class Product {
    constructor(productId, productName, price, stock, seller, detail, img){

        this.productId = productId;
        this.productName = productName;
        this.price = price;
        this.stock = stock;
        this.seller = seller;
        this.detail = detail;
        this.img = img;
    
    
    }


    static fromJson(jsonObj){
        return new Product(
            jsonObj.productId,
            jsonObj.productName,
            jsonObj.price,
            jsonObj.stock,
            jsonObj.seller,
            jsonObj.detail,
            jsonObj.img
        );
    }

    static fromJsonList(jsonObjs){
        const results = [];
        jsonObjs.forEach(jsonObj => {
            results.push(this.fromJson(jsonObj));
        });
        return results;
    }
}

export default Product;