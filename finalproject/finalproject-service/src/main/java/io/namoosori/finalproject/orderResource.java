package io.namoosori.finalproject;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.namoosori.finalproject.dto.OrderDto;
import io.namoosori.finalproject.service.OrderService;

@RequestMapping("/order")
@RestController
public class orderResource {
	
	private final OrderService orderService;
	
	public orderResource(OrderService orderService) {
		this.orderService = orderService;
	}
	@PostMapping("/create-order")
	public void registerOrder(@RequestBody OrderDto orderDto) {
		orderService.registerOrder(orderDto);
	}
	@GetMapping("/find")
	public OrderDto findOrder(@RequestParam int orderNum) {
		return orderService.findOrder(orderNum);
	}
	@GetMapping("/find-email")
	public List<OrderDto> findOrderByEmail(@RequestParam String userEmail) {
		return orderService.findOrderByEmail(userEmail);
	}
	@PostMapping("/modify")
	public void modify(@RequestBody OrderDto orderDto) {
		orderService.modify(orderDto);
	}
	@GetMapping("/remove")
	public void remove(@RequestParam int orderNum) {
		orderService.remove(orderNum);
	}

}
