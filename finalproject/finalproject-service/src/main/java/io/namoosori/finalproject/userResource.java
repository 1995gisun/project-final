package io.namoosori.finalproject;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.namoosori.finalproject.dto.UserDto;
import io.namoosori.finalproject.entity.CustomUser;
import io.namoosori.finalproject.service.UserService;

@RequestMapping("/user")
@RestController
public class userResource {
	
	private final UserService userService;
	
	public userResource(UserService userService) {
		this.userService = userService;
	}
	
	@PostMapping("/create")
	public void registerUser(@RequestBody UserDto userDto) {
		userService.registerUser(userDto);
	}

//	@PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
	@GetMapping("/find-email")
	public UserDto findOnlyEmail(String email) {
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		((CustomUser)auth.getPrincipal()).getEmail();
		return userService.findOnlyEmail(email);
	}
	
	@GetMapping("/find")
	public UserDto findUserByEmail(String email, String password) {
		return userService.findUserByEmail(email, password);
	}
	@PostMapping("/modify")
	public void modify(@RequestBody UserDto userDto) {
		userService.modify(userDto);
	}
	@GetMapping("/remove")
	public void remove(String email) {
		userService.remove(email);
	}

}
