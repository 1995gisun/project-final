package io.namoosori.finalproject;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.namoosori.finalproject.dto.ProductDto;
import io.namoosori.finalproject.service.ProductService;

@RequestMapping("/product")
@RestController
public class productResource {
	
	private final ProductService productService;

	public productResource(ProductService productService) {
		this.productService = productService;
	}
	
	@PostMapping("/create")
	public void registerProduct(@RequestBody ProductDto productDto) {
		productService.registerProduct(productDto);
	}
	@GetMapping("/find-id")
	public ProductDto findProduct(@RequestParam int productId) {
		return productService.findProduct(productId);
	}
	@GetMapping("/find-name")
	public ProductDto findProductByName(@RequestParam String productName) {
		return productService.findProductByName(productName);
	}
	@GetMapping("/find-product-list")
	public List<ProductDto> findProductList(){
		return productService.findProductList();
	}
	@PostMapping("/modify")
	public void modify(@RequestBody ProductDto productDto) {
		productService.modify(productDto);
	}
	@GetMapping("/remove")
	public void remove(@RequestParam int productId) {
		productService.remove(productId);
	}
	
	
}
