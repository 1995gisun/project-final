package io.namoosori.finalproject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.namoosori.finalproject.entity.OrderDetail;
import io.namoosori.finalproject.service.OrderDetailService;

@RequestMapping("/orderDetail")
@RestController
public class orderDetailResource {
	
	private final OrderDetailService orderDetailService;
	
	public orderDetailResource(OrderDetailService orderDetailService) {
		this.orderDetailService = orderDetailService;
	}
	@PostMapping("/create-order")
	public void registerOrderDetail(@RequestBody OrderDetail orderDetail) {
		orderDetailService.registerOrderDetail(orderDetail);
	}
	@GetMapping("/find")
	public OrderDetail findOrderDetail(@RequestParam int orderNum) {
		return orderDetailService.findOrderDetail(orderNum);
	}
	@PostMapping("/modify")
	public void modify(@RequestBody OrderDetail orderDetail) {
		orderDetailService.modify(orderDetail);
	}
	@GetMapping("/remove")
	public void remove(@RequestParam int orderNum) {
		orderDetailService.remove(orderNum);
	}

}
