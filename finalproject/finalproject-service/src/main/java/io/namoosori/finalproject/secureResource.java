package io.namoosori.finalproject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.namoosori.finalproject.entity.CustomUser;
import io.namoosori.finalproject.service.SecureService;


@RestController
@RequestMapping("/public")
public class secureResource {
	
	private final SecureService secureService;
	
	public secureResource(SecureService secureService) {
		this.secureService = secureService;
	}
	
	@PostMapping("/join")
	public void joinUser(@RequestBody CustomUser customUser) {
		secureService.join(customUser);
	}
	@PostMapping("/login")
	public String login(@RequestBody CustomUser customUser) {
		return secureService.login(customUser);
	}

}
