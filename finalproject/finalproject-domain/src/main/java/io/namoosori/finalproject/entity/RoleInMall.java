package io.namoosori.finalproject.entity;

public enum RoleInMall {

	member,
	admin,
	seller
}
