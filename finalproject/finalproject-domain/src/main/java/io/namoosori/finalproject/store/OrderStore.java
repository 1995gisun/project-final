package io.namoosori.finalproject.store;

import java.util.List;

import io.namoosori.finalproject.dto.OrderDto;


public interface OrderStore {
	
	public int create(OrderDto orderDto);
	public OrderDto retrieve(int orderNum);
	public List<OrderDto> retrieveByEmail(String userEmail);
	public void update(OrderDto orderDto);
	public void delete(int orderNum);

}
