package io.namoosori.finalproject.service;

import io.namoosori.finalproject.dto.UserDto;

public interface UserService {
	
	public void registerUser(UserDto userDto);
	public UserDto findOnlyEmail(String email);
	public UserDto findUserByEmail(String email, String password);
	public void modify(UserDto userDto);
	public void remove(String email);

}
