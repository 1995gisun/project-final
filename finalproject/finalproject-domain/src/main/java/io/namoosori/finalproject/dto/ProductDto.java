package io.namoosori.finalproject.dto;

import io.namoosori.finalproject.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
	
	private String productId;
	private String productName;
	private String price;
	private String stock;
	private String seller;
	private String detail;
	private String img;

	public ProductDto(String productId) {
		
		this.productId = productId;
	}
	
	public ProductDto(Product p) {
		this();
//		productId = p.getProductId();
//		productName = p.getProductName();
//		price = p.getPrice();
//		stock = p.getStock();
//		seller = p.getSeller();
//		detail = p.getDetail();
//		img = p.getImg();
	
	}
}
