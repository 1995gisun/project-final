package io.namoosori.finalproject.service;

import io.namoosori.finalproject.entity.OrderDetail;
import io.namoosori.finalproject.entity.Product;

public interface OrderDetailService {
	
	public void registerOrderDetail(OrderDetail orderDetail);
	public OrderDetail findOrderDetail(int orderNum);
	public void modify(OrderDetail orderDetail);
	public void remove(int orderNum);

}
