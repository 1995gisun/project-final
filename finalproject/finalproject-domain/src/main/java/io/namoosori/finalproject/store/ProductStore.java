package io.namoosori.finalproject.store;

import java.util.List;

import io.namoosori.finalproject.dto.ProductDto;

public interface ProductStore {

	public int create(ProductDto productDto);
	public ProductDto retrieve(int productId);
	public ProductDto retrieveByName(String productName);
	public List<ProductDto> retrieveProductList();
	public void update(ProductDto productDto);
	public void delete(int productId);
	
}
