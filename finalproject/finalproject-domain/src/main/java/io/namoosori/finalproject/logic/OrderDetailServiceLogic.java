package io.namoosori.finalproject.logic;

import org.springframework.stereotype.Service;

import io.namoosori.finalproject.entity.OrderDetail;
import io.namoosori.finalproject.service.OrderDetailService;
import io.namoosori.finalproject.store.OrderDetailStore;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderDetailServiceLogic implements OrderDetailService{
	
	private final OrderDetailStore orderDetailStore;
	
	@Override
	public void registerOrderDetail(OrderDetail orderDetail) {
		// TODO Auto-generated method stub
//		String userEmail = orderDetail.getUser().getEmail();
//		int productId = orderDetail.getProduct().getProductId();
		
		orderDetailStore.create(orderDetail);
		
	}

	@Override
	public OrderDetail findOrderDetail(int orderNum) {
		// TODO Auto-generated method stub
		return orderDetailStore.retrieve(orderNum);
	}

	@Override
	public void modify(OrderDetail orderDetail) {
		// TODO Auto-generated method stub
		orderDetailStore.update(orderDetail);
	}

	@Override
	public void remove(int orderNum) {
		// TODO Auto-generated method stub
		orderDetailStore.delete(orderNum);
	}

}
