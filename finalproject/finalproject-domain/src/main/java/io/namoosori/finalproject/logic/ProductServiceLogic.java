package io.namoosori.finalproject.logic;

import java.util.List;

import org.springframework.stereotype.Service;

import io.namoosori.finalproject.dto.ProductDto;
import io.namoosori.finalproject.service.ProductService;
import io.namoosori.finalproject.store.ProductStore;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProductServiceLogic implements ProductService{
	
	private final ProductStore productStore;

	@Override
	public void registerProduct(ProductDto productdto) {
		// TODO Auto-generated method stub
		productStore.create(productdto);
	}

	@Override
	public ProductDto findProduct(int productId) {
		// TODO Auto-generated method stub
		return productStore.retrieve(productId);
	}

	@Override
	public ProductDto findProductByName(String productName) {
		// TODO Auto-generated method stub
		return productStore.retrieveByName(productName);
	}

	@Override
	public List<ProductDto> findProductList() {
		// TODO Auto-generated method stub
		return productStore.retrieveProductList();
	}

	@Override
	public void modify(ProductDto productDto) {
		// TODO Auto-generated method stub
		productStore.update(productDto);
	}

	@Override
	public void remove(int productId) {
		// TODO Auto-generated method stub
		productStore.delete(productId);
	}
	
	


}
