package io.namoosori.finalproject.dto;

import java.util.List;

import io.namoosori.finalproject.entity.Order;
import io.namoosori.finalproject.entity.RoleInMall;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private String email;
    private String password;
    private String phoneNumber;
    private String address;
    private RoleInMall role;
    
//    private List<Order> OrderList;
    
//    public UserDto() {
//    	
//    }

}
