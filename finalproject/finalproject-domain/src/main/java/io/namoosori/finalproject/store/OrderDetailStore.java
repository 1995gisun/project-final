package io.namoosori.finalproject.store;

import io.namoosori.finalproject.entity.OrderDetail;
import io.namoosori.finalproject.entity.Product;
import io.namoosori.finalproject.entity.User;

public interface OrderDetailStore {
	
	public int create(OrderDetail orderDetail);
	public OrderDetail retrieve(int orderNum);
	public void update(OrderDetail orderDetail);
	public void delete(int orderNum);

}
