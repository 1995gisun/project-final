package io.namoosori.finalproject.logic;

import org.springframework.stereotype.Service;

import io.namoosori.finalproject.dto.UserDto;
import io.namoosori.finalproject.service.UserService;
import io.namoosori.finalproject.store.UserStore;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceLogic implements UserService{
	
	private final UserStore userStore;

	@Override
	public void registerUser(UserDto userDto) {
		// TODO Auto-generated method stub
		userStore.create(userDto);
	}

	@Override
	public UserDto findOnlyEmail(String email) {
		// TODO Auto-generated method stub
		return userStore.retrieveOnlyEmail(email);
	}

	@Override
	public UserDto findUserByEmail(String email, String password) {
		// TODO Auto-generated method stub
		return userStore.retrieveByEmail(email, password);
	}

	@Override
	public void modify(UserDto userDto) {
		// TODO Auto-generated method stub
		userStore.update(userDto);
	}

	@Override
	public void remove(String email) {
		// TODO Auto-generated method stub
		userStore.delete(email);
	}

	



}
