package io.namoosori.finalproject.logic;

import java.util.List;

import org.springframework.stereotype.Service;

import io.namoosori.finalproject.dto.OrderDto;
import io.namoosori.finalproject.service.OrderService;
import io.namoosori.finalproject.store.OrderStore;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderServiceLogic implements OrderService{
	
	private final OrderStore orderStore;

	@Override
	public void registerOrder(OrderDto orderDto) {
		// TODO Auto-generated method stub
		orderStore.create(orderDto);
	}

	@Override
	public OrderDto findOrder(int orderNum) {
		// TODO Auto-generated method stub
		return orderStore.retrieve(orderNum);
	}
	
	@Override
	public List<OrderDto> findOrderByEmail(String userEmail) {
		// TODO Auto-generated method stub
		
		return orderStore.retrieveByEmail(userEmail);
	}

	@Override
	public void modify(OrderDto orderDto) {
		// TODO Auto-generated method stub
		orderStore.update(orderDto);
	}

	@Override
	public void remove(int orderNum) {
		// TODO Auto-generated method stub
		orderStore.delete(orderNum);
	}


	
	
	
	

}
