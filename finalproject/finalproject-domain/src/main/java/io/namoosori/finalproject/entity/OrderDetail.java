package io.namoosori.finalproject.entity;

import io.namoosori.finalproject.util.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail {

	private Integer orderNum;
	
	private String userEmail;
	private Product product;
	private Integer productId;

    private Integer status;	// 0:구매완료(기본), 1:상품준비, 2:배송중

    private Integer count;

    private Integer totalPrice;

    private String address;

    private long date;
    
    public OrderDetail(String userEmail, Product product) {
    	
    	this.userEmail = userEmail;
    	this.product = product;
    	
    	this.productId = product.getProductId();
    	
    	this.status = 0;
    	this.count = 1;
    	this.totalPrice = 1;
    	this.address = "test";
    	this.date = Long.parseLong(DateUtil.today());
//    	this.date = 2021;
    	
    }
    
    
    public OrderDetail(String userEmail, Product product, int status, int count, int totalPrice, String address, long date) {
    	
    	this.userEmail = userEmail;
    	this.product = product;
    	
    	this.status = status;
    	this.count = count;
    	this.totalPrice = totalPrice;
    	this.address = address;
//    	this.date = Long.parseLong(DateUtil.today());
    	this.date = date;
    	
    }
    
    public String toString() {
    	StringBuilder builder = new StringBuilder();

    	builder.append("orderNum :").append(orderNum);
//		builder.append(", User email:").append(user.getEmail());
//		builder.append(", product Id:").append(product.getProductId());
//		builder.append("product Name:").append(product.getProductName());
		
		builder.append(", status:").append(status);
		builder.append(", count:").append(count);
		builder.append(", totalPrice:").append(totalPrice);
		builder.append(", address:").append(address);
		builder.append(", date:").append(date);

		return builder.toString();
    }
    
    
}
