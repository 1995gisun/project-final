package io.namoosori.finalproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product {
	
    private String productId;
    private String productName;
    private String price;
    private String stock;
    private String seller;
    private String detail;
    private String img;

    

    public String toString() {
    	StringBuilder builder = new StringBuilder();
    	
    	builder.append("productName:").append(productName);
    	builder.append(", price:").append(price);
    	builder.append(", stock:").append(stock);
    	
    	return builder.toString();
    }

}
