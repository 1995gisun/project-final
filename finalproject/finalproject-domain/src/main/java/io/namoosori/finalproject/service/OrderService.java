package io.namoosori.finalproject.service;

import java.util.List;

import io.namoosori.finalproject.dto.OrderDto;


public interface OrderService {
	
	public void registerOrder(OrderDto orderDto);
	public OrderDto findOrder(int orderNum);
	public List<OrderDto> findOrderByEmail(String userEmail);
	public void modify(OrderDto orderDto);
	public void remove(int orderNum);

}
