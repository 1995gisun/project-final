package io.namoosori.finalproject.entity;

import io.namoosori.finalproject.util.exception.InvalidEmailException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class User {

    private String email;
    private String password;
    private String phoneNumber;
    private String address;
    private RoleInMall role;
    
    public User(String email, String password, String phoneNumber, String address) {
    	this.email = email;
    	this.password = password;
    	this.phoneNumber = phoneNumber;
    	this.address = address;
    	this.role = RoleInMall.member;
    }
    
    public String toString() {
    	StringBuilder builder = new StringBuilder();

		builder.append("email:").append(email);
		builder.append(", password:").append(password);
		builder.append(", phoneNumber:").append(phoneNumber);
		builder.append(", address:").append(address);
		builder.append(", role:").append(role);

		return builder.toString();
    }
    
	public void setEmail(String email) throws InvalidEmailException {
		//
		if (!this.isValidEmailAddress(email)) {
			throw new InvalidEmailException("Email is not valid. --> " + email); 
		}
		
		this.email = email;
	}
    private boolean isValidEmailAddress(String email) {
    	//
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
}
