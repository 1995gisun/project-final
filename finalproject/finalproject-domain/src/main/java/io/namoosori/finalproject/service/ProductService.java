package io.namoosori.finalproject.service;

import java.util.List;

import io.namoosori.finalproject.dto.ProductDto;

public interface ProductService {

	public void registerProduct(ProductDto productdto);
	public ProductDto findProduct(int productId);
	public ProductDto findProductByName(String productName);
	public List<ProductDto> findProductList();
	public void modify(ProductDto productDto);
	public void remove(int productId);
}
