package io.namoosori.finalproject.store;

import io.namoosori.finalproject.dto.UserDto;

public interface UserStore {
	
	public int create(UserDto userDto);
	public UserDto retrieveOnlyEmail(String email);
	public UserDto retrieveByEmail(String email, String password);
	public void update(UserDto userDto);
	public void delete(String email);
	
}
