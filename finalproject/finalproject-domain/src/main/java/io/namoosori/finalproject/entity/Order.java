package io.namoosori.finalproject.entity;

import io.namoosori.finalproject.util.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Order {

	private String orderNum;
	private String userEmail;
	private String productId;
    private String status;	// 0:구매완료(기본), 1:상품준비, 2:배송중
    private String count;
    private String totalPrice;
    private String address;
    private String date;
    
 
    

    

}
