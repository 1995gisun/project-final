package io.namoosori.finalproject.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import io.namoosori.finalproject.dto.ProductDto;

@Mapper
public interface ProductMapper {

	int create(ProductDto productDto);
	ProductDto retrieve(int productId);
	ProductDto retrieveByName(String productName);
	List<ProductDto> retrieveProductList();
	void update(ProductDto productDto);
	void delete(int productId);
	
}
