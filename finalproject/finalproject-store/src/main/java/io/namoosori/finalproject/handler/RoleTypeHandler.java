package io.namoosori.finalproject.handler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import io.namoosori.finalproject.util.JsonUtil;

public class RoleTypeHandler extends BaseTypeHandler<List<String>>{

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, List<String> parameter, JdbcType jdbcType)
			throws SQLException {
		// TODO Auto-generated method stub
		ps.setString(i, JsonUtil.toJson(parameter));
	}

	@Override
	public List<String> getNullableResult(ResultSet rs, String columnName) throws SQLException {
		// TODO Auto-generated method stub
		return Optional.ofNullable(rs.getString(columnName))
				.map(json -> JsonUtil.fromJsonList(json, String.class))
				.orElse(new ArrayList<>());
	}

	@Override
	public List<String> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return Optional.ofNullable(rs.getString(columnIndex))
				.map(json -> JsonUtil.fromJsonList(json, String.class))
				.orElse(new ArrayList<>());
	}

	@Override
	public List<String> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return Optional.ofNullable(cs.getString(columnIndex))
				.map(json -> JsonUtil.fromJsonList(json, String.class))
				.orElse(new ArrayList<>());
	}

}
