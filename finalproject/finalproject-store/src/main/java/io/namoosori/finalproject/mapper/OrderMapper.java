package io.namoosori.finalproject.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import io.namoosori.finalproject.dto.OrderDto;

@Mapper
public interface OrderMapper {
	
	int create(OrderDto orderDto);
	OrderDto retrieve(int orderNum);
	List<OrderDto> retrieveByEmail(String userEmail);

	void update(OrderDto orderDto);
	void delete(int orderNum);
}
