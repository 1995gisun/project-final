package io.namoosori.finalproject.mapper;

import org.apache.ibatis.annotations.Mapper;

import io.namoosori.finalproject.entity.CustomUser;

@Mapper
public interface SecureMapper {

	void create(CustomUser customUser);
	CustomUser retrieve(String email);
}
