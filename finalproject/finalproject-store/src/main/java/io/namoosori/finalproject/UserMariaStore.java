package io.namoosori.finalproject;

import org.springframework.stereotype.Repository;

import io.namoosori.finalproject.dto.UserDto;
import io.namoosori.finalproject.mapper.UserMapper;
import io.namoosori.finalproject.store.UserStore;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class UserMariaStore implements UserStore{
	
	private final UserMapper mapper;

	@Override
	public int create(UserDto userDto) {
		// TODO Auto-generated method stub
		return mapper.create(userDto);
	}

	@Override
	public UserDto retrieveOnlyEmail(String email) {
		// TODO Auto-generated method stub
		return mapper.retrieveOnlyEmail(email);
	}

	@Override
	public UserDto retrieveByEmail(String email, String password) {
		// TODO Auto-generated method stub
		return mapper.retrieveByEmail(email, password);
	}

	@Override
	public void update(UserDto userDto) {
		// TODO Auto-generated method stub
		mapper.update(userDto);
	}

	@Override
	public void delete(String email) {
		// TODO Auto-generated method stub
		mapper.delete(email);
	}



}
