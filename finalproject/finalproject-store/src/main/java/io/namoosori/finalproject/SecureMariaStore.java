package io.namoosori.finalproject;

import org.springframework.stereotype.Repository;

import io.namoosori.finalproject.entity.CustomUser;
import io.namoosori.finalproject.mapper.SecureMapper;
import io.namoosori.finalproject.store.SecureStore;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@Repository
public class SecureMariaStore implements SecureStore{

	private final SecureMapper mapper;
	
	@Override
	public String create(CustomUser customUser) {
		// TODO Auto-generated method stub
		 mapper.create(customUser);
		 return " ㅊㅋㅊㅋ";
	}
	@Override
	public CustomUser retrieve(String email) {
		// TODO Auto-generated method stub
		return mapper.retrieve(email);
	}



}
