package io.namoosori.finalproject;

import org.springframework.stereotype.Repository;

import io.namoosori.finalproject.entity.OrderDetail;
import io.namoosori.finalproject.mapper.OrderDetailMapper;
import io.namoosori.finalproject.store.OrderDetailStore;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class OrderDetailMariaStore implements OrderDetailStore{
	
	private final OrderDetailMapper mapper;
	
	@Override
	public int create(OrderDetail orderDetail) {
		// TODO Auto-generated method stub
		return mapper.create(orderDetail);
	}

	@Override
	public OrderDetail retrieve(int orderNum) {
		// TODO Auto-generated method stub
		return mapper.retrieve(orderNum);
	}

	@Override
	public void update(OrderDetail orderDetail) {
		// TODO Auto-generated method stub
		mapper.update(orderDetail);
	}

	@Override
	public void delete(int orderNum) {
		// TODO Auto-generated method stub
		mapper.delete(orderNum);
	}



}
