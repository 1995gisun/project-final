package io.namoosori.finalproject.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import io.namoosori.finalproject.dto.UserDto;

@Mapper
public interface UserMapper {

	int create(UserDto userDto);
	UserDto retrieveOnlyEmail(String email);
	UserDto retrieveByEmail(@Param("email") String email, @Param("password") String password);
	int update(UserDto userDto);
	int delete(String email);
	
}
