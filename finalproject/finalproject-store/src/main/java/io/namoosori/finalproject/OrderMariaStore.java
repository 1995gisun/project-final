package io.namoosori.finalproject;

import java.util.List;

import org.springframework.stereotype.Repository;

import io.namoosori.finalproject.dto.OrderDto;
import io.namoosori.finalproject.mapper.OrderMapper;
import io.namoosori.finalproject.store.OrderStore;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class OrderMariaStore implements OrderStore{
	
	private final OrderMapper mapper;

	@Override
	public int create(OrderDto orderDto) {
		// TODO Auto-generated method stub
		return mapper.create(orderDto);
	}

	@Override
	public OrderDto retrieve(int orderNum) {
		// TODO Auto-generated method stub
		return mapper.retrieve(orderNum);
	}
	
	@Override
	public List<OrderDto> retrieveByEmail(String userEmail) {
		// TODO Auto-generated method stub
		return mapper.retrieveByEmail(userEmail);
	}

	@Override
	public void update(OrderDto orderDto) {
		// TODO Auto-generated method stub
		mapper.update(orderDto);
	}

	@Override
	public void delete(int orderNum) {
		// TODO Auto-generated method stub
		mapper.delete(orderNum);
	}





}
