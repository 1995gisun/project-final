package io.namoosori.finalproject;

import java.util.List;

import org.springframework.stereotype.Repository;

import io.namoosori.finalproject.dto.ProductDto;
import io.namoosori.finalproject.mapper.ProductMapper;
import io.namoosori.finalproject.store.ProductStore;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class ProductMariaStore implements ProductStore{
	
	private final ProductMapper mapper;

	@Override
	public int create(ProductDto productDto) {
		// TODO Auto-generated method stub
		return mapper.create(productDto);
	}

	@Override
	public ProductDto retrieve(int productId) {
		// TODO Auto-generated method stub
		return mapper.retrieve(productId);
	}

	@Override
	public ProductDto retrieveByName(String productName) {
		// TODO Auto-generated method stub
		return mapper.retrieveByName(productName);
	}

	@Override
	public List<ProductDto> retrieveProductList() {
		// TODO Auto-generated method stub
		return mapper.retrieveProductList();
	}

	@Override
	public void update(ProductDto productDto) {
		// TODO Auto-generated method stub
		mapper.update(productDto);
	}

	@Override
	public void delete(int productId) {
		// TODO Auto-generated method stub
		mapper.delete(productId);
	}
	



}
