package io.namoosori.finalproject.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import io.namoosori.finalproject.entity.OrderDetail;

@Mapper
public interface OrderDetailMapper {
	
	int create(OrderDetail orderDetail);
	OrderDetail retrieve(int orderNum);
	void update(OrderDetail orderDetail);
	void delete(int orderNum);
}
