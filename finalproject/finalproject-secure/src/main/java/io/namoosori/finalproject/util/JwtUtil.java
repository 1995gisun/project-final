package io.namoosori.finalproject.util;

import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
public class JwtUtil {
	@Value("${token.secretKey}")
	private String secretKey;
	
	private final UserDetailsService userDetailsService;
	
	private long accessTokenValidTime = TimeUnit.HOURS.toMillis(1);		// 1 hour
	private long refreshTokenValidTime = TimeUnit.DAYS.toMillis(7);
	
	
	
	
	
//	public void init() { secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes(standard))	}
	
	public String createAccessToken(String user, List<String> roles) {
		Claims claims = Jwts.claims().setSubject(user);
		claims.put("roles", roles);
		Date now = new Date();
		return Jwts.builder()
				.setClaims(claims)
				.setIssuedAt(now)
				.setExpiration(new Date(now.getTime()+ accessTokenValidTime))
				.signWith(SignatureAlgorithm.HS256, secretKey)
				.compact();
	}
	
	public Authentication getAuthentication(HttpServletRequest request) {		// 인증된 객체, 해당 스레드에 대해서 secret spring? 제공 만족
		String token = request.getHeader("AUTH-TOKEN");
		UserDetails userDetails = userDetailsService.loadUserByUsername(this.getUsername(token));
		return new UsernamePasswordAuthenticationToken(userDetails, "",
				userDetails.getAuthorities());
	}
	
	public String getUsername(String token) {
		return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
	}
	
	public boolean validateToken(HttpServletRequest request) {
		String token = request.getHeader("AUTH-TOKEN");
		if(token == null)	return false;
		try {
			Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
		} catch (Exception e) {
			request.setAttribute("exception", e.getClass().getSimpleName());
			return false;
		}
		return true;
	}
	
	
	/*
	 * public static void main(String[] args) { JwtUtil util = new JwtUtil() String
	 * token = }
	 */

}
