package io.namoosori.finalproject.store;

import io.namoosori.finalproject.entity.CustomUser;

public interface SecureStore {
	
	String create(CustomUser customUser);
	CustomUser retrieve(String email);

}
