package io.namoosori.finalproject.secure;


import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

 

import io.namoosori.finalproject.util.JwtUtil;
import lombok.RequiredArgsConstructor;


@EnableWebSecurity
@RequiredArgsConstructor
public class webSecurityConfig extends WebSecurityConfigurerAdapter{
	
	private final JwtUtil jwtUtil;
	
	private static final String[] AUTH_WHITELIST = {
            // — Swagger UI v2
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/swagger-ui.html/**",
            "/webjars/**",
            // — Swagger UI v3 (OpenAPI)
            "/v3/api-docs/**",
            "/swagger-ui/**"
            // other public endpoints of your API may be appended to this array
    };
	
	@Bean
	public PasswordEncoder passwordEncodr() {
		//
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		
	http
			.csrf().disable()
			.httpBasic().disable()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
			.authorizeRequests()
			.antMatchers(AUTH_WHITELIST).permitAll()
//			.antMatchers("/public/**").permitAll()
			.antMatchers("/user/**").permitAll()
			.antMatchers("/product/**").permitAll()
			.antMatchers("/order/**").permitAll()
			.anyRequest().authenticated()
		.and()
		// TODO
//			.exceptionHandling()
//				.authenticationEntryPoint(jwtExceptionHandler)
//				.accessDeniedHandler(jwtAccessDeniedHandler)
//			.and()
//			.addFilterBefore(new JwtFilter(jwtUtil), UsernamePasswordAuthenticationFilter.class);
//			.addfilterBefore(new JwtFilter(JwtUtil, UsernamePasswordAuthenticationFilter.class));
			.addFilterBefore(new JwtFilter(jwtUtil), UsernamePasswordAuthenticationFilter.class);
//			.addFilterBefore(new JwtFilter(jwtUtil), UsernamePasswordAuthenticationFilter.class);
			
	}
	

	



	
}
